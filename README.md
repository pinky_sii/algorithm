# Algorithm Analysis #
Tri 2/2017-2018

* Week 01 Asymtotic Analysis
* Week 02 Recurrences, Divide-and-Conquer Algorithm
* Week 03 Probabilistic Analysis, Randomized Algorithm
* Week 04 Graph and Graph Algorithms
* Week 05 Greedy Algorithms 
* Week 06 Greedy Algorithms (cont.)
* Week 07 Dinamic Programming 
* Week 08 Greedy Algorithms 
* Week 09 Greedy Algorithms (cont.)
* Week 10 Amortized Analysis
* Week 11 NP-Completeness
* Week 12 Network Flow Algorithms 
