package com.company;

import java.util.Scanner;

/**
 * Created by Siripatsornsirichai on 11/28/2017 AD.
 */
public class Coupon {

    public static int canPurchase(int[] coupon, int n, int k) {
        int[][] m = new int[k+1][n+1];
        for(int i = 0; i<=n; i++) {
            m[0][i] = 1;
        }
        for(int i=1; i<=k; i++) {
            m[i][0] = 0;
        }

        for (int i=1; i<=k; i++) {
            for(int j =1; j<=n; j++) {
                m[i][j] = m[i][j-1];
                if (i >= coupon[j-1])
                    m[i][j] = Math.max(m[i][j], m[i - coupon[j-1]][j-1]);
            }
        }
        return m[k][n];
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();                   // the item price
        int n = in.nextInt();                   // the number of coupons
        int[] coupon = new int[n];
        for(int i=0; i<n; i++) {
            coupon[i] = in.nextInt();
        }
        int ans = canPurchase(coupon,n,k);
        if (ans==1) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }

    }
}
