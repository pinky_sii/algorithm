package com.company;

import java.util.Scanner;

/**
 * Created by Siripatsornsirichai on 11/28/2017 AD.
 */
public class Mcs {
    public static void maxConSub(int[] arr, int n) {
        int ending_sum = 0;
        int max_so_far = Integer.MIN_VALUE;
        int start = 0;
        int end = 0;
        int s = 0;

        for(int i=0; i< n; i++) {
            if(arr[i] > ending_sum+arr[i]){
                ending_sum = arr[i];
                s = i;
            } else {
                ending_sum = ending_sum+arr[i];
            }
            if(ending_sum > max_so_far) {
                max_so_far = ending_sum;
                start = s;
                end = i;
            }
        }
//        for(int i = start; i<=end;i++) {
//            System.out.print(arr[i] + " ");
//        }
        if (max_so_far < 0) {
            System.out.println(0);
        } else System.out.println(max_so_far);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int i=0; i<n; i++) {
            arr[i] = in.nextInt();
        }
        maxConSub(arr,n);
    }


}
